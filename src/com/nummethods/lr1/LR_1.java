package com.nummethods.lr1;

/**
 * @author aNNiMON
 */
public class LR_1 {

    public static void main(String[] args) {
        System.out.println("=== 1 ===");
        Epsilon.getEpsilon();
        MaxNumber.getMaxNumber();
        System.out.println("\n=== 2 ===");
        Taylor.getTaylor();
    }
}
