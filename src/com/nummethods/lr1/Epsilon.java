package com.nummethods.lr1;

/**
 * Calculate epsilon.
 * @author aNNiMON
 */
public class Epsilon {
    
    public static void getEpsilon() {
        System.out.println("Float epsilon: " + getEpsilonFloat());
        System.out.println("Float constant epsilon: " + Float.MIN_VALUE);
        System.out.println("Double epsilon: " + getEpsilonDouble());
        System.out.println("Double constant epsilon: " + Double.MIN_VALUE);
    }
    
    private static float getEpsilonFloat() {
        float num = 1f;
        float prevNumber = num;
        while (num != 0f) {//1 + num != 1f) {
            prevNumber = num;
            num /= 2f;
        }
        return prevNumber;
    }
    
    private static double getEpsilonDouble() {
        double num = 1.0;
        double prevNumber = 1.0;
        while (num != 0.0) {
            prevNumber = num;
            num /= 2.0;
        }
        return prevNumber;
    }
}
