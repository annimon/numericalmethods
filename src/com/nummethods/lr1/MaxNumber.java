package com.nummethods.lr1;

/**
 * Calculate max number.
 * @author aNNiMON
 */
public class MaxNumber {

    public static void getMaxNumber() {
        System.out.println("Float max number:   " + getMaxNumberFloat());
        System.out.println("Float constant max: " + Float.MAX_VALUE);
        System.out.println("Double max number:   " + getMaxNumberDouble());
        System.out.println("Double constant max: " + Double.MAX_VALUE);
    }
    
    private static float getMaxNumberFloat() {
        float num = 1f;
        float prevNumber = 1f;
        while (num != Float.POSITIVE_INFINITY) {
            prevNumber = num;
            num *= 2f;
        }
        return prevNumber;
    }
    
    private static double getMaxNumberDouble() {
        double num = 1.0;
        double prevNumber = 1.0;
        while (num != Double.POSITIVE_INFINITY) {
            prevNumber = num;
            num *= 2.0;//1.001
        }
        return prevNumber;
    }
}
