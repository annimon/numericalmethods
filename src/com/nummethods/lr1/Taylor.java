package com.nummethods.lr1;

/**
 * Calculate Taylor series
 * @author aNNiMON
 */
public class Taylor {
    
    public static void getTaylor() {
        final int x = 2;
        System.out.println("Taylor (long):   " + taylorLongFactorial(x));
        System.out.println("Taylor (double): " + taylorDoubleFactorial(x));
        System.out.println("Taylor (w/o factorial long):   " + taylorWithoutFactorialLong(x));
        System.out.println("Taylor (w/o factorial double): " + taylorWithoutFactorialDouble(x));
    }
    
    private static double taylorLongFactorial(int x) {
        long sum = 0;
        long tempSum;
        int n = 0;
        do {
            int temp = 2 * n + 1;
            tempSum = (long) Math.pow(-1, n);
            tempSum *= Math.pow(x, temp);
            tempSum /= factorialLong(n) * temp;
            
            sum += tempSum;
            n++;
        } while(tempSum != 0);
        System.out.println("N=" + n);
        double sum2 = sum *  (2.0 / Math.sqrt(Math.PI));
        
        return sum2;
    }
    
    private static double taylorDoubleFactorial(int x) {
        double sum = 0.0;
        double tempSum;
        int n = 0;
        do {
            int temp = 2 * n + 1;
            tempSum = Math.pow(-1, n);
            tempSum *= Math.pow(x, temp);
            tempSum /= factorialDouble(n) * temp;
            
            sum += tempSum;
            n++;
        } while(tempSum != 0);
        System.out.println("N=" + n);
        sum *= (2.0 / Math.sqrt(Math.PI));
        
        return sum;
    }
    
    private static double taylorWithoutFactorialLong(int x) {
        long sum = 0;
        long tempSum = x;
        int n = 0;
        do {
            sum += tempSum;
            n++;
            
            tempSum *= -1 * (2 * n - 1) * (x * x);
            tempSum /= n * (2 * n + 1);
        } while(tempSum != 0);
        System.out.println("N=" + n);
        
        double sum2 = sum * (2.0 / Math.sqrt(Math.PI));
        
        return sum2;
    }
    
    private static double taylorWithoutFactorialDouble(int x) {
        double sum = 0.0;
        double tempSum = x;
        int n = 0;
        do {
            sum += tempSum;
            n++;
            
            tempSum *= -1 * (2 * n - 1) * (x * x);
            tempSum /= n * (2 * n + 1);
        } while(Math.abs(tempSum) != 0);
        System.out.println("N=" + n);
        sum *= (2.0 / Math.sqrt(Math.PI));
        
        return sum;
    }

    private static long factorialLong(int n) {
        long factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }
    
    private static double factorialDouble(int n) {
        double factorial = 1.0;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }
}
