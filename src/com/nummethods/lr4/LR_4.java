package com.nummethods.lr4;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * v10
 * cos(x)*exp(sin(x))
 * 0    PI
 * @author aNNiMON
 */
public class LR_4 extends JFrame {
    
    private static final Integral var10 = new Integral(new Function() {

            @Override
            public double f(double x) {
                return Math.cos(x) * Math.exp(Math.sin(x));
            }
        }, 0, Math.PI, 0.005); // 3.4365
    
    private static final Integral var3 = new Integral(new Function() {

            @Override
            public double f(double x) {
                return Math.log(x*x*x);
            }
        }, 2, 10, 0.0002); // 40.9187
    
    private static final Integral test = new Integral(new Function() {

            @Override
            public double f(double x) {
                return (x * x) * Math.sin(x);
            }
        }, 0, 1, 10e-6);// 0.22324
    

    public static void main(String[] args) {
        new LR_4().setVisible(true);
    }

    public LR_4() {
        super("LR_4");
        setBounds(300, 120, 0, 0);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        SimpsonIntegrate simpson = new SimpsonIntegrate(var10);
        System.out.println( simpson.calculate(6) );
        
        GraphicPanel panel = new GraphicPanel(simpson);
        panel.setPreferredSize(new Dimension(400, 300));
        add(panel);
        
        pack();
    }
    
}
