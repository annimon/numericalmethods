package com.nummethods.lr4;

import java.awt.Color;
import java.awt.Graphics;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * @author aNNiMON
 */
public class GraphicPanel extends javax.swing.JPanel {
    
    private SimpsonIntegrate simpson;
    
    public GraphicPanel(SimpsonIntegrate simpson) {
        this.simpson = simpson;
        
        setBackground(Color.WHITE);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        final int width = g.getClipBounds().width;
        final int height = g.getClipBounds().height;
        
        // ������ ������������ ������������
        Integral integral = simpson.getIntegral();
        double xMin = Math.min(integral.getA(), integral.getB());
        double xMax = Math.max(integral.getA(), integral.getB());
        double yMin = Math.min(integral.f(xMin), integral.f(xMax)) - 1;
        double yMax = Math.max(integral.f(xMin), integral.f(xMax)) + 1;
        double dx = width / (xMax - xMin);
        double dy = height / (yMax - yMin);
        
        // ����������������� �������
        double xStep = (xMax - xMin) / width;
        for (double x = xMin; x <= xMax; x += xStep) {
            g.setColor(Color.GREEN);
            plot(g, (x - xMin) * dx, height - (integral.f(x) - yMin) * dy);
        }
        
        // ���������
        g.setColor(Color.RED);
        ArrayList<Double> parts = simpson.getParts();
        for (int i = 0; i < parts.size(); i++) {
            Double xx = parts.get(i);
            double x = xx;// / dx + xMin;
            int graphX = (int) ((x - xMin) * dx);
            g.drawLine(graphX, 0, graphX, height);
            String part = new DecimalFormat("#.##").format(xx);
            g.drawString(part, graphX, (i % 2 == 0) ? 10 : height - 20);
        }
        
        // �������
        g.setColor(Color.BLACK);
        g.drawString("Integral: " + simpson.calculate(6), 10, 20);
        g.drawString("Parts: " + parts.size(), 10, 40);
    }
    
    private void plot(Graphics g, double x, double y) {
        g.drawLine((int) x, (int) y, (int) x, (int) y);
    }

}
