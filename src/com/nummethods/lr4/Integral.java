package com.nummethods.lr4;

/**
 * @author aNNiMON
 */
public class Integral implements Function {
    
    private Function func;
    private double a, b;
    private double epsilon;

    public Integral(Function func, double a, double b, double epsilon) {
        this.func = func;
        this.a = a;
        this.b = b;
        this.epsilon = epsilon;
    }

    @Override
    public double f(double x) {
        return func.f(x);
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getEpsilon() {
        return epsilon;
    }
    
}
