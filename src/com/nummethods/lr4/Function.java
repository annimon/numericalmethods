package com.nummethods.lr4;

/**
 * @author aNNiMON
 */
public interface Function {
   
    double f(double x);
}
