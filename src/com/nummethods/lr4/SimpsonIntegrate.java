package com.nummethods.lr4;

import java.util.ArrayList;

/**
 *
 * @author aNNiMON
 */
public class SimpsonIntegrate {
    
    private Integral integral;
    private ArrayList<Double> part;

    public SimpsonIntegrate(Integral integral) {
        this.integral = integral;
        
        part = new ArrayList<Double>();
    }
    
    public double calculate(int step) {
        part.clear();
        part.add(integral.getA());
        
        double result = 0;
        double h = (integral.getB() - integral.getA()) / step;
        for (int i = 0; i < step; i++) {
            double from = integral.getA() + h * i;
            double partWithBigH = part(from, from + h, h);
            double hDecreased = h;
            
            boolean needToDecreaseH;
            double partWithSmallH;
            do {
                partWithSmallH = part(from, from + h, hDecreased / 2);
                needToDecreaseH = !checkRunge(partWithBigH, partWithSmallH) && (hDecreased > 0.001);
                if (needToDecreaseH) {
                    hDecreased /= 2;
                }
            } while (needToDecreaseH);
            
            result += Math.abs(partWithSmallH);
            for(double k = hDecreased; k <= h; k += hDecreased) {
                part.add(from + k);
            }
        }
        
        return result;
    }
    
    public Integral getIntegral() {
        return integral;
    }
    
    public ArrayList<Double> getParts() {
        return part;
    }
    
    private double part(double a, double b, double h) {
        double result = 0;
        for (double i = a; i < b; i += h) {
            result += integral(i, i + h);
        }
        return result;
    }
    
    private double integral(double x1, double x2) {
        final double h = x2 - x1;
        return h/6 * (integral.f(x1) + 4 * integral.f(x1 + h/2) + integral.f(x2));
    }
    
    private boolean checkRunge(double i1, double i2) {
            return (Math.abs(i1 - i2)) < 15 * integral.getEpsilon();
    }
    
}
