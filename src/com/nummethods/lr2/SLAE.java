package com.nummethods.lr2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Read and calculate SLAE by gauss method.
 * @author aNNiMON
 */
public class SLAE {
    
    private Matrix A;
    private Vector d;
    
    public SLAE(String resource) {
        try {
            read(getClass().getResourceAsStream(resource));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public Vector gauss() {
        int rank = A.getRank();
        for (int row = 0; row < rank; row++) {
            int best = row;
            for (int i = row + 1; i < rank; i++) {
                if (Math.abs(A.get(best, row)) < Math.abs(A.get(i, row))) {
                    best = i;
                }
            }
            
            A.swap(row, best);
            d.swap(row, best);
            
            for (int i = row + 1; i < rank; i++) {
                double value = A.get(row, i) / A.get(row, row);
                A.set(row, i, value);
                // a[row][i] /= a[row][row];
            }
            if (Math.abs(A.get(row, row)) <= 1e-9) System.out.println("������� �����������");
            d.set(row, d.get(row) / A.get(row, row));
            for (int i = 0; i < rank; i++) {
                double x = A.get(i, row);
                if (i != row && x != 0) {
                    for (int j = row + 1; j < rank; j++) {
                        double value = A.get(i, j) - A.get(row, j) * x;
                        A.set(i, j, value);
                    }
                    d.set(i, d.get(i) - d.get(row) * x);
                }
            }
        }
        
        for (int i = 0; i < A.getRank(); i++) {
            double y = 0;
            for (int j = 0; j < A.getRank(); j++) {
                y += A.get(i, j) * d.get(j);
            }
            if (Math.abs(d.get(i) - y) < 1e-9) {
                System.err.println("������");
                break;
            }
        }
        return d;
    }
    
    private void read(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        final int rank = Integer.parseInt(getNonCommentString(reader));
        // Read matrix.
        Matrix matrix = new Matrix(rank);
        for (int y = 0; y < rank; y++) {
            Vector v = getVectorFromString(getNonCommentString(reader));
            matrix.setLine(y, v);
        }
        //System.out.print(matrix.toString());
        Vector vector = getVectorFromString( getNonCommentString(reader) );
        //System.out.println("\n" + vector.toString());
        
        reader.close();
        
        A = matrix;
        d = vector;
    }
    
    private String getNonCommentString(BufferedReader reader) throws IOException {
        while (true) {            
            String line = reader.readLine();
            if (line == null) return "";
            if (!line.startsWith(";")) return line;
        }
    }
    
    private Vector getVectorFromString(String line) {
        String[] items = line.split(",");
        Vector vector = new Vector(items.length);
        for (int i = 0; i < items.length; i++) {
            double value = Double.parseDouble(items[i].trim() );
            vector.set(i, value);
        }
        return vector;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Source SLAE");
        for (int i = 0; i < A.getRank(); i++) {
            sb.append('\n');
            sb.append(A.getLine(i).toString()).append("=\t").append(d.get(i));
        }
        
        return sb.toString();
    }
    
}
