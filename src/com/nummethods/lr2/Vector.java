package com.nummethods.lr2;

/**
 *
 * @author aNNiMON
 */
public class Vector {
    
    private double[] coeff;

    public Vector(int rank) {
        coeff = new double[rank];
    }
    
    public Vector(double[] row) {
        coeff = new double[row.length];
        System.arraycopy(row, 0, coeff, 0, row.length);
    }
    
    public double[] get() {
        return coeff;
    }
    
    public double get(int x) {
        return coeff[x];
    }
    
    public void set(int x, double value) {
        coeff[x] = value;
    }
    
    public void swap(int i1, int i2) {
        double temp = coeff[i1];
        coeff[i1] = coeff[i2];
        coeff[i2] = temp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        final int rank = coeff.length;
        for (int i = 0; i < rank; i++) {
            sb.append(coeff[i]).append("\t");
        }
        return sb.toString();
    }
}
