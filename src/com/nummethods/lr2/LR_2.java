package com.nummethods.lr2;

/**
 * @author aNNiMON
 */
public class LR_2 {
    
    public static void main(String[] args) {
        final String name = "v10";
        SLAE slae = new SLAE("/com/nummethods/lr2/" + name + ".slae");
        System.out.println(slae.toString());
        Vector answer = slae.gauss();
        System.out.println("\nAnswer: " + answer.toString());
    }
    
}
