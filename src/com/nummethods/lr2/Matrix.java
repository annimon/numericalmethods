package com.nummethods.lr2;

/**
 * @author aNNiMON
 */
public class Matrix {
    
    private double[][] coeff;

    public Matrix(int rank) {
        coeff = new double[rank][rank];
    }
    
    public double[][] get() {
        return coeff;
    }
    
    public int getRank() {
        return coeff.length;
    }
    
    public double get(int y, int x) {
        return coeff[y][x];
    }
    
    public void set(int y, int x, double value) {
        coeff[y][x] = value;
    }
    
    public Vector getLine(int y) {
        Vector v = new Vector(coeff[y]);
        return v;
    }
    
    public void setLine(int y, Vector row) {
        coeff[y] = row.get();
    }
    
    public void swap(int i1, int i2) {
        double[] temp = coeff[i1];
        coeff[i1] = coeff[i2];
        coeff[i2] = temp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        final int rank = coeff.length;
        for (int i = 0; i < rank; i++) {
            for (int j = 0; j < rank; j++) {
                sb.append(coeff[i][j]).append("\t");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
    
    
}
