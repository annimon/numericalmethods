package com.nummethods.lr3;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import javax.swing.JFrame;

/**
 * @author aNNiMON
 */
public class LR_3 extends JFrame {

    public static void main(String[] args) {
        new LR_3().setVisible(true);
    }

    public LR_3() {
        super("LR_3");
        setBounds(300, 120, 0, 0);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        Point2D[] input = new Point2D[] {
            new Point2D.Double(1, 2.0),
            new Point2D.Double(2, 2.3),
            new Point2D.Double(3, 2.9),
            new Point2D.Double(4, 3.5),
            new Point2D.Double(5, 4.2),
            new Point2D.Double(6, 6.0),
            new Point2D.Double(7, 8.1),
            new Point2D.Double(8, 10.7)
        };
        GraphicPanel panel = new GraphicPanel(input);
        panel.setPreferredSize(new Dimension(400, 300));
        panel.setInterpolations(new Lagrange(input), new CubicSpline(input));
        add(panel);
        
        pack();
    }
    
}
