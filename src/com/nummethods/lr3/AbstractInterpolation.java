package com.nummethods.lr3;

import java.awt.geom.Point2D;

/**
 *
 * @author aNNiMON
 */
public abstract class AbstractInterpolation {

    protected Point2D[] input;
    
    public AbstractInterpolation(Point2D[] input) {
        this.input = input;
    }
    
    public abstract double interpolate(double x);
}
