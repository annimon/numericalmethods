package com.nummethods.lr3;

import java.awt.geom.Point2D;

public class Lagrange extends AbstractInterpolation {

    public Lagrange(Point2D[] input) {
        super(input);
    }

    @Override
    public double interpolate(double x) {
        double y = 0;
        for (int i = 0; i < input.length; i++) {
            y += calculateMember(i, x);
        }
        return y;
    }
    
    private double calculateMember(int idx, double x) {
        // ���������
        double m1 = 1;
        for (int i = 0; i < input.length; i++) {
            if (i != idx) {
                m1 *= (x - input[i].getX());
            }
        }
        // �����������
        double m2 = 1;
        for (int i = 0; i < input.length; i++) {
            if (i != idx) {
                m2 *= (input[idx].getX() - input[i].getX());
            }
        }

        return (m1 / m2) * input[idx].getY();
    }

}
