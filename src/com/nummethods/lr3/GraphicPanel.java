package com.nummethods.lr3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * @author aNNiMON
 */
public class GraphicPanel extends javax.swing.JPanel {
    
    private Point2D[] input;
    private Lagrange lagrange;
    private CubicSpline cubicSpline;
    
    private Point clickedPoint;
    
    public GraphicPanel(Point2D[] input) {
        this.input = input;
        setBackground(Color.WHITE);
        addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            @Override
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        clickedPoint = new Point(0, 0);
    }
    
    public void setInterpolations(Lagrange lagrange, CubicSpline cubicSpline) {
        this.lagrange = lagrange;
        this.cubicSpline = cubicSpline;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        final int width = g.getClipBounds().width;
        final int height = g.getClipBounds().height;
        
        // ������ ������������ ������������
        double xMin = input[0].getX();
        double xMax = input[0].getX();
        double yMin = input[0].getY();
        double yMax = input[0].getY();
        for (int i = 1; i < input.length; i++) {
            Point2D point = input[i];
            if (xMin > point.getX()) xMin = point.getX();
            else if (xMax < point.getX()) xMax = point.getX();
            if (yMin > point.getY()) yMin = point.getY();
            else if (yMax < point.getY()) yMax = point.getY();
        }
        double dx = width / (xMax - xMin);
        double dy = height / (yMax - yMin);
        
        // ����������������� �������
        double xStep = (xMax - xMin) / width;
        for (double x = xMin; x <= xMax; x += xStep) {
            g.setColor(Color.GREEN);
            plot(g, (x - xMin) * dx, height - (lagrange.interpolate(x) - yMin) * dy);
            
            g.setColor(Color.BLUE);
            plot(g, (x - xMin) * dx, height - (cubicSpline.interpolate(x) - yMin) * dy);
        }
        
        // ����������� ��������� �����
        g.setColor(Color.RED);
        for (int i = 0; i < input.length; i++) {
            Point2D point = input[i];
            int x = (int) ( (point.getX() - xMin) * dx);
            int y = (int) (height - (point.getY() - yMin) * dy);
            g.fillRect(x, y, 3, 3);
            g.drawString(point.getX() + ", " + point.getY(), x + 2, y - 4);
        }
        
        // ����������� ������� ��������� �����
        double x = clickedPoint.getX() / dx + xMin;
        int graphX = (int) ((x - xMin) * dx);
        g.drawLine(graphX, 0, graphX, height);
        
        // �������
        g.setColor(Color.BLACK);
        g.drawString("x: " + x, 10, 20);
        g.setColor(Color.GREEN);
        g.drawString("Lagrange: " + lagrange.interpolate(x), 10, 40);
        g.setColor(Color.BLUE);
        g.drawString("Cubic Spline: " + cubicSpline.interpolate(x), 10, 60);
    }
    
    private void formMousePressed(java.awt.event.MouseEvent evt) {                                  
        clickedPoint = evt.getPoint();
        repaint();
    } 
    
    private void plot(Graphics g, double x, double y) {
        g.drawLine((int) x, (int) y, (int) x, (int) y);
    }

}
