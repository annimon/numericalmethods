package com.nummethods.lr3;

import java.awt.geom.Point2D;

public class CubicSpline extends AbstractInterpolation {
    
    private SplineTuple[] splines;
 
    public CubicSpline(Point2D[] input) {
        super(input);
        buildSpline();
    }
    
    @Override
    public double interpolate(double x) {
        if (splines == null) {
            buildSpline();
        }
 
        final int n = splines.length;
        SplineTuple s;
 
        if (x <= splines[0].x) {
             // ���� x ������ ����� ����� x[0] - ���������� ������ ��-��� �������
            s = splines[1];
        } else if (x >= splines[n - 1].x) {
            // ���� x ������ ����� ����� x[n - 1] - ���������� ��������� ��-��� �������
            s = splines[n - 1];
        } else {
            // ����� x ����� ����� ���������� ������� ����� - ���������� �������� ����� ������� ��-�� �������
            int i = 0;
            int j = n - 1;
            while (i + 1 < j) {
                int k = i + (j - i) / 2;
                if (x <= splines[k].x) {
                    j = k;
                } else {
                    i = k;
                }
            }
            s = splines[j];
        }
 
        double dx = x - s.x;
        // ��������� �������� ������� � �������� �����.
        return s.a + (s.b + (s.c / 2.0 + s.d * dx / 6.0) * dx) * dx; 
    }

    private void buildSpline() {
        final int n = input.length;
        // ������������� ������� ��������
        splines = new SplineTuple[n];
        for (int i = 0; i < n; i++) {
            splines[i] = new SplineTuple();
            splines[i].x = input[i].getX();
            splines[i].a = input[i].getY();
        }
        splines[0].c = splines[n - 1].c = 0.0;
 
        // ������� ���� ������������ ������������� �������� c[i] ������� �������� ��� ���������������� ������
        // ���������� ����������� ������������� - ������ ��� ������ ��������
        double[] alpha = new double[n - 1];
        double[] beta  = new double[n - 1];
        alpha[0] = beta[0] = 0.0;
        for (int i = 1; i < n - 1; i++) {
            double hi  = input[i].getX() - input[i - 1].getX();
            double hi1 = input[i + 1].getX() - input[i].getX();
            double A = hi;
            double C = 2.0 * (hi + hi1);
            double B = hi1;
            double F = 6.0 * ((input[i + 1].getY() - input[i].getY()) /
                    hi1 - (input[i].getY() - input[i - 1].getY()) / hi);
            double z = (A * alpha[i - 1] + C);
            alpha[i] = -B / z;
            beta[i] = (F - A * beta[i - 1]) / z;
        }
 
        // ���������� ������� - �������� ��� ������ ��������
        for (int i = n - 2; i > 0; i--) {
            splines[i].c = alpha[i] * splines[i + 1].c + beta[i];
        }
 
        // �� ��������� ������������� c[i] ������� �������� b[i] � d[i]
        for (int i = n - 1; i > 0; i--) {
            double hi = input[i].getX() - input[i - 1].getX();
            splines[i].d = (splines[i].c - splines[i - 1].c) / hi;
            splines[i].b = hi * (2.0 * splines[i].c + splines[i - 1].c) / 6.0 + (input[i].getY() - input[i - 1].getY()) / hi;
        }
    }
    
    // ���������, ����������� ������ �� ������ �������� �����
    private class SplineTuple {
        double a, b, c, d, x;
    }

}
