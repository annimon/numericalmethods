package com.nummethods.lr6;

import com.nummethods.lr4.Function;

/**
 *
 * @author aNNiMON
 */
public class LR_6 {
    
    private final Function func = var10;
    
    private static final Function var2 = new Function() {

        @Override
        public double f(double x) {
            return Math.cos(x) * Math.cos(x) - (x * x);
        }
    };
    
    private static final Function var3 = new Function() {

        @Override
        public double f(double x) {
            return Math.pow(x, 7) - Math.pow(x, 5) - x + 3;
        }
    };
    
    private static final Function var10 = new Function() {

        @Override
        public double f(double x) {
            return x - Math.pow(Math.E, -x);
        }
    };

    public static void main(String[] args) {
        new LR_6().calc();
    }
    
    private final double a = -10, b = 10;
    private final double h = 0.001, epsilon = 0.0001;

    public void calc() {
        double result = calcBisection(a, b);
        System.out.println("����� ��������: x = " + result);
        System.out.println("0 = " + func.f(result));
        
        result = calcNewton( (a + b) / 2 );
        System.out.println("����� �������: x = " + result);
        System.out.println("0 = " + func.f(result));
    }
    
    /** ---- ����� �������� ---- */
    private double calcBisection(double a, double b) {
        double x = (a + b) / 2;
        if (func.f(x) == 0) return x;
        else if ((b - a) > epsilon) {
            if (func.f(a) * func.f(x) < 0) return calcBisection(a, x);
            else return calcBisection(x, b);
        }
        return (a + b) / 2;
    }
    
    /** ---- ����� ������� ---- */
    private double calcNewton(double x) {
        double a = x - (func.f(x) / derivate(x));
        return (Math.abs(func.f(a)) > epsilon) ? calcNewton(a) : x;
    }

    private double derivate(double x) {
        return (func.f(x + h) - func.f(x - h)) / (2 * h);
    }
}
