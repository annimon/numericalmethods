package com.nummethods.lr5;

import java.awt.geom.Point2D;

/**
 * ����� ������.
 * @author aNNiMON
 */
public class EulerMethod extends IntegrateMethod {

    public EulerMethod(Integral integral) {
        super(integral);
    }

    @Override
    protected double integrate(double xi, double h, double x0, double v0) {
        double vi = (isEquals(xi, x0)) ? v0 : integrate(xi-h, h, x0, v0);
        double result = vi + h * integral.f(xi, vi);
        points.add(new Point2D.Double(xi, result));
        return result;
    }
    
}
