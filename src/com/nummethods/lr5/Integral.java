package com.nummethods.lr5;

/**
 * @author aNNiMON
 */
public class Integral implements Function {
    
    private Function func;
    private final double v0, x0, xN;
    private final int N;
    
    public Integral(Function func, double v0, double x0, double xN, int N) {
        this.func = func;
        this.v0 = v0;
        this.x0 = x0;
        this.xN = xN;
        this.N = N;
    }
    
    public void setFunc(Function func) {
        this.func = func;
    }
    
    @Override
    public double f(double x, double v) {
        return func.f(x, v);
    }

    public double getX0() {
        return x0;
    }

    public double getXN() {
        return xN;
    }

    public double getV0() {
        return v0;
    }

    public int getN() {
        return N;
    }
    
}
