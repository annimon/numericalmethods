package com.nummethods.lr5;

import java.awt.geom.Point2D;

/**
 * ����� ������.
 * @author aNNiMON
 */
public class NormalMethod extends IntegrateMethod {

    public NormalMethod(Integral integral) {
        super(integral);
    }

    @Override
    public double calculate(int n) {
        double h = (integral.getXN() - integral.getX0()) / n;
        for (double x = integral.getX0(); x <= integral.getXN(); x += h) {
            points.add(new Point2D.Double(x, integral.f(x, integral.getV0())));
        }
        
        /*for (int i = 0; i < n; i++) {
            double x = integral.getX0() + h * i;
            points.add(new Point2D.Double(x, integral.f(x, integral.getV0())));
        }*/
       
        return 0;
    }
    
    @Override
    protected double integrate(double xi, double h, double x0, double v0) {
        /*if (isEquals(xi, x0)) {
            points.add(new Point2D.Double(xi, integral.f(xi, v0)));
        } else integrate(xi - h, h, x0, v0);*/
        return 0;
    }
    
}
