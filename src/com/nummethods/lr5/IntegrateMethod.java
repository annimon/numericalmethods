package com.nummethods.lr5;

import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * @author aNNiMON
 */
public abstract class IntegrateMethod {
    
    protected final Integral integral;
    protected final ArrayList<Point2D> points;
    
    public IntegrateMethod(Integral integral) {
        this.integral = integral;
        this.points = new ArrayList<Point2D>();
    }
    
    public double calculate(int n) {
        double h = (integral.getXN() - integral.getX0()) / n;
        return integrate(integral.getXN(), h, integral.getX0(), integral.getV0());
    }
    
    protected abstract double integrate(double xi, double h, double x0, double v0);
    
    public Integral getIntegral() {
        return integral;
    }
    
    public Point2D[] getPoints() {
        Point2D[] array = new Point2D[points.size()];
        array = points.toArray(array);
        return array;
    }
    
    protected boolean isEquals(double a, double b) {
        return Math.abs(a - b) < 0.00001;
    }
}
