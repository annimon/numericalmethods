package com.nummethods.lr5;

/**
 * @author aNNiMON
 */
public interface Function {
   
    double f(double x, double v);
}
