package com.nummethods.lr5;

import java.awt.geom.Point2D;

/**
 * ����� �����-�����
 * @author aNNiMON
 */
public class RungeKuttaMethod extends IntegrateMethod {

    public RungeKuttaMethod(Integral integral) {
        super(integral);
    }

    @Override
    public double integrate(double xi, double h, double x0, double v0) {
        double vi = isEquals(xi, x0) ? v0 : integrate(xi-h, h, x0, v0);
        double k1 = k1(h, xi, vi);
        double k2 = k2(h, xi, vi, k1);
        double k3 = k3(h, xi, vi, k2);
        double k4 = k4(h, xi, vi, k3);

        double result = vi + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
        points.add(new Point2D.Double(xi, result));
        return result;
    }
    
    private double k1(double h, double x, double v) {
        return h * integral.f(x, v);
    }

    private double k2(double h, double x, double v, double k1) {
        return h * integral.f(x + h / 2, v + k1 / 2);
    }

    private double k3(double h, double x, double v, double k2) {
        return h * integral.f(x + h / 2, v + k2 / 2);
    }

    private double k4(double h, double x, double v, double k3) {
        return h * integral.f(x + h / 2, v + k3 / 2);
    }
    
}
