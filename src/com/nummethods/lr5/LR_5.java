package com.nummethods.lr5;

import com.nummethods.lr3.CubicSpline;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import javax.swing.JFrame;
import util.GraphicPanel;

/**
 *
 * @author aNNiMON
 */
public class LR_5 extends JFrame {
    
    private static final Integral var2 = new Integral(new Function() {

            @Override
            public double f(double x, double v) {
                return Math.sin(x);
            }
        }, 0, -Math.PI, Math.PI, 50); // -0.006 | 4.386
    
    private static final Integral var10 = new Integral(new Function() {

            @Override
            public double f(double x, double v) {
                return Math.log(x);
            }
        }, 0, 1, 5, 100); // 4.106 | 4.079
    
    private static final Integral var12 = new Integral(new Function() {

            @Override
            public double f(double x, double v) {
                return 1 / (x);
            }
        }, 0, 1, 5, 75);
    
    public static void main(String[] args) {
        new LR_5().setVisible(true);
    }

    public LR_5() {
        super("LR_5");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        final Integral integral = var2;
        
        IntegrateMethod runge = new RungeKuttaMethod(integral);
        System.out.println("Runge-Kutta: " + runge.calculate(integral.getN()));
        final Point2D[] rungePoints = runge.getPoints();
        
        IntegrateMethod euler = new EulerMethod(integral);
        System.out.println("Euler: " + euler.calculate(integral.getN()));
        final Point2D[] eulerPoints = euler.getPoints();
        
        Function f10 = new Function() {

            @Override
            public double f(double x, double v) {
                return x * Math.log(x) - x + 1;
            }
        };
        
        Function f2 = new Function() {

            @Override
            public double f(double x, double v) {
                return -Math.cos(x) - 1;
            }
        };

        final Point2D[] normalPoints = new Point2D[rungePoints.length];
        for (int i = 0; i < normalPoints.length; i++) {
            double x = rungePoints[i].getX();
            double y = f2.f(x, 0);
            normalPoints[i] = new Point2D.Double(x, y);
        }
        
        GraphicPanel panel = new GraphicPanel() {
            
            private int xOld, yOld;

            @Override
            protected void drawLabels(Graphics g, double x) {
                g.setColor(Color.BLACK);
                g.drawString("x: " + x, 10, 20);
                g.setColor(Color.BLUE);
                g.drawString("Runge-Kutta: " + findPoint(rungePoints, x), 10, 40);
                g.setColor(new Color(0x00C000));
                g.drawString("Euler: " + findPoint(eulerPoints, x), 10, 60);
                g.setColor(new Color(0xD35E2C));
                g.drawString("Normal: " + findPoint(normalPoints, x), 10, 80);
            }

            @Override
            protected void plot(Graphics g, double x, double y, boolean firstIteration) {
                if (firstIteration) {
                    // ������ �������� - ���������� ����������
                    xOld = (int) x;
                    yOld = (int) y;
                    return;
                }
                g.fillRect(xOld - 1, yOld - 1, 3, 3);
                g.drawLine(xOld, yOld, (int) x, (int) y);
                xOld = (int) x;
                yOld = (int) y;
            }
        };
        panel.addPoints(rungePoints);
        panel.addPoints(eulerPoints);
        panel.addPoints(normalPoints);
        
        
        panel.setPreferredSize(new Dimension(400, 300));
        add(panel);
        
        pack();
        setLocationRelativeTo(null);
    }
    
    private double findPoint(Point2D[] array, double x) {
        CubicSpline spline = new CubicSpline(array);
        return spline.interpolate(x);
        /*for (int i = 0; i < array.length; i++) {
            double dx = Math.abs( array[i].getX() - x );
            if (dx < 0.001) {
                return array[i].getY();
            }
        }
        return 0;*/
    }
}
