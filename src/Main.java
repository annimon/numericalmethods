
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

/**
 * ����� ����, ����������� ������������ ������.
 * @author aNNiMON
 */
public class Main extends JFrame {
    
    private static final int NUM_OF_LABS = 6;
    
    public static void main(String[] args) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {  }

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new Main().setVisible(true);
            }
        });
        
    }

    public Main() {
        setTitle("��������� ������");
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        JPanel panel = new JPanel();
        initPanel(panel);
        
        getContentPane().add(panel);
        pack();
        setLocationRelativeTo(null);
    }

    private void initPanel(JPanel panel) {
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        for (int i = 1; i <= NUM_OF_LABS; i++) {
            panel.add(createButton("������������ ������ �"+i, i));
        }
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
    }
    
    private JButton createButton(final String text, final int num) {
        JButton button = new JButton();
        button.setText(text);
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // ����� ������ main �����������.
                    Class<?> cls = Class.forName("com.nummethods.lr"+num+".LR_"+num);
                    Method meth = cls.getMethod("main", String[].class);
                    meth.invoke(null, (Object) null);
                } catch (Exception cne) {
                    cne.printStackTrace();
                }
            }
        });
        return button;
    }
}
