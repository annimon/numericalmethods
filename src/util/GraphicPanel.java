package util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 * @author aNNiMON
 */
public abstract class GraphicPanel extends JPanel {
    
    // ����� ��� ��������.
    private static final Color[] COLORS = {
        Color.BLUE, new Color(0x00C000), new Color(0xD35E2C), Color.MAGENTA
    };
    
    private final ArrayList<Point2D[]> pointsHolder;
    private Point clickedPoint;
    
    public GraphicPanel() {
        pointsHolder = new ArrayList<Point2D[]>();
        setBackground(Color.WHITE);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        clickedPoint = new Point(0, 0);
    }
    
    public void addPoints(Point2D[] input) {
        pointsHolder.add(input);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        final int width = g.getClipBounds().width;
        final int height = g.getClipBounds().height;
        
        if (pointsHolder.size() <= 0) return;
        
        // ������ ������������ ������������
        Point2D[] input = pointsHolder.get(0);
        double xMin = input[0].getX();
        double xMax = input[0].getX();
        double yMin = input[0].getY();
        double yMax = input[0].getY();
        
        for (int j = 0; j < pointsHolder.size(); j++) {
            input = pointsHolder.get(j);
            for (int i = 0; i < input.length; i++) {
                Point2D point = input[i];
                if (xMin > point.getX()) xMin = point.getX();
                else if (xMax < point.getX()) xMax = point.getX();
                if (yMin > point.getY()) yMin = point.getY();
                else if (yMax < point.getY()) yMax = point.getY();
            }
        }
        
        double dx = width / (xMax - xMin);
        double dy = height / (yMax - yMin);
        
        // ��������� ��������
        for (int j = 0; j < pointsHolder.size(); j++) {
            g.setColor(COLORS[j]);
            input = pointsHolder.get(j);
            
            for (int i = 0; i < input.length; i++) {
                Point2D p = input[i];
                plot(g, (p.getX() - xMin) * dx, height - (p.getY() - yMin) * dy, i == 0);
            }
        }
        
        // ����������� ������� ��������� �����
        g.setColor(Color.RED);
        double x = clickedPoint.getX() / dx + xMin;
        int graphX = (int) ((x - xMin) * dx);
        g.drawLine(graphX, 0, graphX, height);
        
        // �������
        drawLabels(g, x);
    }
    
    protected abstract void drawLabels(Graphics g, double x);
    
    protected void plot(Graphics g, double x, double y, boolean firstIteration) {
        g.drawLine((int) x, (int) y, (int) x, (int) y);
    }
    
    private void formMousePressed(java.awt.event.MouseEvent evt) {                                  
        clickedPoint = evt.getPoint();
        repaint();
    } 
    
}
